# README #

* Cassie Chin
* cassiechin9793@gmail.com

### What is this repository for? ###

* CSCE 4600
* Homework 4
* Question 4

### How do I get set up? ###

* `echo 0 > file.txt`
* `gcc q4.c`
* `./a.out file.txt`

### Problem Description ###

Write a simple sequence-number system through which two processes, P1 and P2, can each obtain 50 unique integers, such that one receives all the odd and the other all the even numbers. Use the fork() call to create P1 and P2.