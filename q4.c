﻿/**
 * Cassie Chin, 2/19/15
 * CSCE 4600, Homework 2, Question 4
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static int *glob_c1;
static int *glob_c2;
static int *glob_will_wait;

/**
 * Read and write to the critical section.
 * @param filename The file to edit
 */
void doFileIO (char *filename) {
  // Open the file, read an integer, and close the file
  FILE *file = fopen (filename, "r");
  if (!file) return;
  int N;   
  fscanf (file, "%d", &N);
  fclose (file);

  printf("n=%d, pid=%d\n", N, getpid()); 
  N = N + 1;

  // Open the file, write the new integer, flush, and close the file
  file = fopen ("file.txt", "w");
  if (!file) return;
  fprintf (file, "%d\n", N);
  fflush (file);
  fclose (file);
}

int main (int argc, char **argv) {
  if (argc != 2) {
    printf("Usage %s [filename]\n", argv[0]);
    return 0;
  }
  // Map global variables
  glob_c1 = mmap (NULL, sizeof *glob_c1, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  glob_c2 = mmap (NULL, sizeof *glob_c2, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  glob_will_wait = mmap (NULL, sizeof *glob_will_wait, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  *glob_c1 = 0;
  *glob_c2 = 0;

  int status_p1, status_p2;
  int p1 = fork();
  if (p1 > 0) {
    // In the parent process still, fork again
    int p2 = fork();
    if (p2 > 0) {
      // Still in the parent process, wait for p2 to finish
      wait (&status_p2);
    }
    else if (p2 == 0) {
      // In child process 2
      int i;
      for (i=0; i<50; i++) {
	*glob_c2 = 1; *glob_will_wait = 2; while (*glob_c1 && *glob_will_wait == 2);
	doFileIO(argv[1]);
	*glob_c2 = 0;
      }
    }

    wait (&status_p1); // wait for process 1 to finish
    munmap(glob_c1, sizeof *glob_c1);
    munmap(glob_c2, sizeof *glob_c2);
    munmap(glob_will_wait, sizeof *glob_will_wait);    
  }
  else if (p1 == 0) {
    // In the child process
    int i;
    for (i=0; i<50; i++) {
      *glob_c1 = 1; *glob_will_wait = 1; while (*glob_c2 && *glob_will_wait == 1);
      doFileIO(argv[1]);
      *glob_c1 = 0;
    }
  }
  else {
    // Error
  }        

  return 0;
}
